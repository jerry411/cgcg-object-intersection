#include "Rasterizer.h"
#include "GraphicsThrowMacros.h"
#include "BindableCodex.h"

namespace Bind
{
	/*Rasterizer::Rasterizer( Graphics& gfx,bool twoSided )
		:
		twoSided( twoSided )
	{
		INFOMAN( gfx );
		
		D3D11_RASTERIZER_DESC rasterDesc = CD3D11_RASTERIZER_DESC( CD3D11_DEFAULT{} );
		rasterDesc.CullMode = twoSided ? D3D11_CULL_NONE : D3D11_CULL_BACK;

		GFX_THROW_INFO( GetDevice( gfx )->CreateRasterizerState( &rasterDesc,&pRasterizer ) );
	}*/

	Rasterizer::Rasterizer(Graphics& gfx, D3D11_CULL_MODE cullMode, D3D11_FILL_MODE fillMode)
	{
		INFOMAN(gfx);

		D3D11_RASTERIZER_DESC rasterDesc = CD3D11_RASTERIZER_DESC(CD3D11_DEFAULT{});
		rasterDesc.CullMode = cullMode;
		rasterDesc.FillMode = fillMode;

		GFX_THROW_INFO(GetDevice(gfx)->CreateRasterizerState(&rasterDesc, &pRasterizer));
	}

	void Rasterizer::Bind( Graphics& gfx ) noexcept
	{
		GetContext( gfx )->RSSetState( pRasterizer.Get() );
	}
	
	/*std::shared_ptr<Rasterizer> Rasterizer::Resolve( Graphics& gfx,bool twoSided )
	{
		return Codex::Resolve<Rasterizer>( gfx,twoSided );
	}*/

	std::shared_ptr<Rasterizer> Rasterizer::Resolve(Graphics& gfx, D3D11_CULL_MODE cullMode, D3D11_FILL_MODE fillMode)
	{
		return Codex::Resolve<Rasterizer>(gfx, cullMode, fillMode);
	}

	std::string Rasterizer::GenerateUID(D3D11_CULL_MODE cullMode, D3D11_FILL_MODE fillMode)
	{
		using namespace std::string_literals;
		return typeid(Rasterizer).name() + "#"s + std::to_string(cullMode) + "_" + std::to_string(fillMode);
	}

	std::string Rasterizer::GetUID() const noexcept
	{
		return GenerateUID(D3D11_CULL_NONE, D3D11_FILL_SOLID);
	}
}