#pragma once
#include <array>

#include "BindableCommon.h"
#include "Graphics.h"
#include "Job.h"
#include "Pass.h"
#include "PerfLog.h"

class FrameCommander
{
public:
	void Accept(Job job, size_t target) noexcept
	{
		passes[target].Accept(job);
	}
	
	void Execute(Graphics& gfx) const noxnd
	{
		using namespace Bind;
		// normally this would be a loop with each pass defining it setup / etc.
		// and later on it would be a complex graph with parallel execution contingent
		// on input / output requirements

		// main phong lighting pass
		Stencil::Resolve(gfx, Stencil::Mode::Off)->Bind(gfx);
		passes[0].Execute(gfx);

		gfx.pContext->ClearDepthStencilView(gfx.pDSV.Get(), D3D11_CLEAR_DEPTH | D3D11_CLEAR_STENCIL, 1.0f, 0u);

		/*// Intersection - write last front face to depth buffer
		Stencil::Resolve( gfx,Stencil::Mode::IntersectionWriteLastFrontFace )->Bind( gfx );
		//PixelShader::Resolve(gfx, "Solid_PS.cso")->Bind(gfx);
		NullPixelShader::Resolve( gfx )->Bind( gfx );
		passes[1].Execute( gfx );

		// Intersection - count back faces
		Stencil::Resolve( gfx,Stencil::Mode::IntersectionCountBackFaces)->Bind( gfx );
		//PixelShader::Resolve(gfx, "Solid_PS.cso")->Bind(gfx);
		NullPixelShader::Resolve(gfx)->Bind(gfx);
		passes[2].Execute( gfx );

		// Intersection - actual draw using depth + stencil mask
		Stencil::Resolve(gfx, Stencil::Mode::IntersectionUseMask)->Bind(gfx);
		passes[3].Execute(gfx);*/


		Stencil::Resolve(gfx, Stencil::Mode::CinderEdgesToDepthBuffer)->Bind(gfx);
		//PixelShader::Resolve(gfx, "Solid_PS.cso")->Bind(gfx);
		NullPixelShader::Resolve(gfx)->Bind(gfx);
		passes[1].Execute(gfx);

		Stencil::Resolve(gfx, Stencil::Mode::CinderCountFaces)->Bind(gfx);
		//PixelShader::Resolve(gfx, "Solid_PS.cso")->Bind(gfx);
		NullPixelShader::Resolve(gfx)->Bind(gfx);
		passes[2].Execute(gfx);

		Stencil::Resolve(gfx, Stencil::Mode::CinderMask)->Bind(gfx);
		PixelShader::Resolve(gfx, "Solid_PS.cso")->Bind(gfx);
		//NullPixelShader::Resolve(gfx)->Bind(gfx);
		passes[3].Execute(gfx);
	}

	void Reset() noexcept
	{
		for (auto& p : passes)
		{
			p.Reset();
		}
	}
	
private:
	std::array<Pass, 4> passes;
};