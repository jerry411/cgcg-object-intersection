# CGCG - Object Intersection
This project represents a semester project for the course "Computer Graphics for Game Development", taught at Charles University, Prague.
Author: Jaroslav Vozár


## Overall goal
*  Rendering system for intersections of objects using DirectX 11 made from scratch.
*  Several rendering modes.
*  Implementation to existing complex renderer.

## Target result
* Versatile system for rendering intersections of objects and implementation into existing renderer.
* Optimization for rendering on low-power devices such as Microsoft HoloLens (1 and 2).

Simple object intersection
<img src="https://forum.unity.com/attachments/cylinders-png.43911/" alt="Simple object intersection" width="400"/>

Intersection of concave object with convex one
<img src="https://forum.unity.com/attachments/donut-png.43912/" alt="Intersection of convex object" width="400"/>

## State of the art
* Several methods for rendering CSG data using stencil buffer.
* [A Z-Buffer CSG Rendering Algorithm for Convex Objects](https://dspace5.zcu.cz/bitstream/11025/15811/1/R41.pdf)
* [Improved CSG Rendering using Overlap Graph Subtraction Sequences](https://www.researchgate.net/publication/2559180_Improved_CSG_Rendering_using_Overlap_Graph_Subtraction_Sequences)

## Initial state
* System as whole will be made from scratch.
* Existing DirectX 11 renderer for later implementation into it.

## Technologies, libraries and frameworks
* Visual Studio 2019
* DirectX 11
* Existing universal DirectX 11 renderer

## Features of this project
Rendering modes of intersections, such as rendering of:
* volumetric intersection with different color (shader)
* nothing but intersections
* outline of intersection
* single-level subtraction of objects from specific object (group of objects) such as floor